function addTokens(input, tokens){
    if ((typeof input === 'string') || (input instanceof String))
    {
        if(input.length>=6)
        {
            
            if(input.includes('...')===true)
            {

                for (let tokenName of tokens) {
                    if(typeof tokenName==='string'||tokenName instanceof String)
                    {
                        input=input.replace( '...',`${tokenName}`)
                    }
                    else
                    {
                        throw new Error('Invalid array format')
                    }
                }
                
            }
            
        }
        else
        {
            throw new Error('Input should have at least 6 characters')
        }
    }
    else
    {
        throw new Error('Invalid input')
    }
    return input
}

const app = {
    addTokens: addTokens
}

module.exports = app;